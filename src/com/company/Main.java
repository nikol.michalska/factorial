package com.company;

public class Main {

    public static void main(String[] args) {
       System.out.println("Factorial of provided number is : " + getFactorial(10));


    }
    public static int getFactorial (int number){
        if(number < 2){
            return 1;
        }
        return number * getFactorial(number - 1);
    }
}
